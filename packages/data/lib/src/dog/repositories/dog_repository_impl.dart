import 'package:dartz/dartz.dart';
import 'package:domain/domain.dart';
import 'package:utils/utils.dart';

/// Implementation of Dog Contract in Data layer
class DogRepositoryImpl implements DogRepository {
  ///Implementation constructor
  DogRepositoryImpl({required this.dogDataSource});

  /// Dog Data source
  final DogDataSource dogDataSource;

  @override
  Future<Either<Failure, List<DogEntity>>> getDogs() async {
    try {
      return Right(await dogDataSource.getDogs());
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
