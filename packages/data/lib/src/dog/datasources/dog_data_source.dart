import 'dart:convert';

import 'package:data/src/dog/model/dog_model.dart';

import 'package:http/http.dart' as http;
import 'package:utils/utils.dart';

/// Abstract Auth class
// ignore: one_member_abstracts
abstract class DogDataSource {
  /// Calls the https://baseUrl/v1/breeds/image/random endpoint.
  ///
  /// Throws a ServerException for all error codes.
  Future<List<DogModel>> getDogs();
}

/// Implementation of abstract class
class DogDataSourceImpl implements DogDataSource {
  /// Constructor that required http client
  DogDataSourceImpl({required this.client});

  /// Http client to make calls
  final http.Client client;

  /// Base URL
  final baseUrl = AppConfiguration.instance.config.apiBaseUrl;

  /// Current API Version
  final apiVersion = AppConfiguration.instance.apiVersion;

  /// API Service
  final breeds = '/breeds';

  @override
  Future<List<DogModel>> getDogs() async {
    try {
      final uri = '$baseUrl$apiVersion$breeds';

      final response = await client.get(
        Uri.parse(uri),
        headers: {'x-api-key': AppConfiguration.instance.apiKey},
      );

      if (response.statusCode == 200) {
        final dynamic json = jsonDecode(response.body);

        return (json as List<dynamic>)
            .map((dynamic e) => DogModel.fromJson(e as Map<String, dynamic>))
            .toList();
      } else {
        throw ServerException();
      }
    } catch (e) {
      throw ServerException();
    }
  }
}
