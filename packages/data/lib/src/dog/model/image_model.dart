import 'package:domain/domain.dart';

/// Image model that extends from ImageEntity
// ignore: must_be_immutable
class Image extends ImageEntity {
  /// Constructor of Image
    Image({
        required this.height,
        required this.id,
        required this.url,
        required this.width,
    }) : super(id: id, height: height, url: url, width: width);

    /// Height of the image
    int height;

    /// Id of the image
    String id;

    /// Url of the image
    String url;

    /// Width of the image
    int width;

    /// Function that parse from Json to Image Model
    // ignore: sort_constructors_first
    factory Image.fromJson(Map<String, dynamic> json) => Image(
        height: json['height'] as int,
        id: json['id'] as String,
        url: json['url'] as String,
        width: json['width'] as int,
    );

    /// Function that parse from Image Model to Json
    Map<String, dynamic> toJson() => <String, dynamic>{
        'height': height,
        'id': id,
        'url': url,
        'width': width,
    };
}
