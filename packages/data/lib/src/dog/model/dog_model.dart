import 'package:data/src/dog/model/image_model.dart';
import 'package:domain/domain.dart';

/// Dog model extends from Dog Entity
class DogModel extends DogEntity {
  /// Dog model constructor
  const DogModel({
    required int id,
    required String name,
    String? description = '',
    required Image image,
  }) : super(id: id, description: description, name: name, image: image);

  /// Function to parse from Entity to Model
  DogModel.castFromEntity(final DogEntity dog)
      : super(
          id: dog.id,
          name: dog.name,
          description: dog.description,
          image: dog.image,
        );

  /// Function to parse from JSON to Dog model
  factory DogModel.fromJson(Map<String, dynamic> json) {
    return DogModel(
      id: json['id'] as int,
      name: json['name'] as String,
      description: (json['bred_for'] != null) ? json['bred_for'] as String : '',
      image: Image.fromJson(json['image'] as Map<String, dynamic>),
    );
  }

  /// Function to parse from Dog model to JSON
  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'description': description,
      'image': image,
    };
  }
}
