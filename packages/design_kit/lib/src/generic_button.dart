import 'package:flutter/material.dart';

/// Custom generic button
class GenericButton extends StatelessWidget {
  /// Constructor of generic button
  const GenericButton({
    Key? key,
    required this.child,
    required this.onTap,
    this.radius = 12,
    this.height = 100,
    this.width = 200,
    this.btnColor = Colors.red,
  }) : super(key: key);

  /// Radius of the container
  final double radius;

  /// Background color of the container
  final Color btnColor;

  /// Height of the container
  final double height;

  /// Width of the container
  final double width;

  /// Child that this button (container) receive
  final Widget child;

  /// Callback of this button
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          color: btnColor,
          borderRadius: BorderRadius.all(Radius.circular(radius)),
        ),
        child: child,
      ),
    );
  }
}
