import 'package:dartz/dartz.dart';
import 'package:domain/src/dog/entities/dog_entity.dart';
import 'package:utils/utils.dart';

/// Dog Repository Contract in Domain layer
// ignore: one_member_abstracts
abstract class DogRepository {
  /// Function that return Failure or an List of dogs
  Future<Either<Failure, List<DogEntity>>> getDogs();
}
