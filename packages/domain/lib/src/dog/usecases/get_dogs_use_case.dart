import 'package:dartz/dartz.dart';
import 'package:domain/src/dog/entities/dog_entity.dart';
import 'package:domain/src/dog/repositories/dog_repository.dart';
import 'package:utils/utils.dart';

/// Use case related to get dogs
class GetDogsUseCase implements UseCase<List<DogEntity>, NoParams> {
  /// GetDogs constructor
  GetDogsUseCase(this.repository);

  /// Dog repository must be mandatory
  final DogRepository repository;

  @override
  Future<Either<Failure, List<DogEntity>>> call(NoParams params) async {
    return repository.getDogs();
  }
}
