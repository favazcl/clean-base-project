import 'package:equatable/equatable.dart';

/// Dog entity class that extends of Equatable package
class ImageEntity extends Equatable {
  /// ImageEntity Constructor
  const ImageEntity({
    required this.height,
    required this.id,
    required this.url,
    required this.width,
  });

  /// ImageEntity id
  final int height;

  /// ImageEntity name
  final String id;

  /// ImageEntity description
  final String url;

  /// ImageEntity image
  final int width;

  @override
  List<Object> get props => [id, height, url, width];
}
