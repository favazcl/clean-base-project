import 'package:domain/src/dog/entities/image_entity.dart';
import 'package:equatable/equatable.dart';

/// Dog entity class that extends of Equatable package
class DogEntity extends Equatable {
  /// DogEntity Constructor
  const DogEntity({
    required this.id,
    required this.name,
    required this.image,
    this.description = '',
  });

  /// DogEntity id
  final int id;

  /// DogEntity name
  final String name;

  /// DogEntity description
  final String? description;

  /// DogEntity image
  final ImageEntity image;

  @override
  List<Object> get props => [id, name, description!, image];
}
