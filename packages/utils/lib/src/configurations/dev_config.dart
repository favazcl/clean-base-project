import 'package:utils/src/configurations/base_config.dart';

/// Dev configurations that implements abstract class
class DevConfig implements BaseConfig {
  @override
  String get apiBaseUrl => 'https://api.thedogapi.com/';
}
