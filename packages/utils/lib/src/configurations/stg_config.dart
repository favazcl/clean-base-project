import 'package:utils/src/configurations/base_config.dart';

/// Stage configurations that implements abstract class
class StgConfig implements BaseConfig {
  @override
  String get apiBaseUrl => 'https://api.thedogapi.com/';
}
