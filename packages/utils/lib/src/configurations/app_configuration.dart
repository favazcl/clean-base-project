import 'package:utils/src/configurations/base_config.dart';
import 'package:utils/src/configurations/dev_config.dart';
import 'package:utils/src/configurations/prod_config.dart';
import 'package:utils/src/configurations/stg_config.dart';
import 'package:utils/src/enums/environment.dart';

/// General App Configurations
class AppConfiguration {
  /// Singleton of AppConfiguration
  static AppConfiguration instance = AppConfiguration();

  /// Current API version
  String apiVersion = '/v1';

  /// API KEY that needs API
  String apiKey = '80b0c7b4-f5aa-4de0-8296-50d5e2ae40b8';

  /// Current Environment
  late Environment env;

  /// Current Base Config
  late BaseConfig config;

  /// Method that set a new environment
  void setEnvironment(Environment environment) {
    env = environment;

    switch (environment) {
      case Environment.dev:
        config = DevConfig();
        break;
      case Environment.stg:
        config = StgConfig();
        break;
      case Environment.prod:
        config = ProdConfig();
        break;
    }
  }
}
