/// Base config class
abstract class BaseConfig {
  /// API base url that will be use to make request
  String get apiBaseUrl;
}
