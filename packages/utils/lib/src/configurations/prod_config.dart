import 'package:utils/src/configurations/base_config.dart';

/// Prod configurations that implements abstract class
class ProdConfig implements BaseConfig {
  @override
  String get apiBaseUrl => 'https://api.thedogapi.com/';
}
