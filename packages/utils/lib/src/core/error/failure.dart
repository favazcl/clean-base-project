
import 'package:equatable/equatable.dart';

/// Abstract class of failures
abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

/// General failures
class ServerFailure extends Failure {}
