/// Generic Server Expection
class ServerException implements Exception {}

/// Generic Cache Exception
class CacheException implements Exception {}
