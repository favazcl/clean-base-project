import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:utils/src/core/error/failure.dart';

/// Generic use case class that will be use to all use cases
// ignore: one_member_abstracts
abstract class UseCase<Type, Params> {
  /// Centralized call that needs Params or not to call new usecases.
  Future<Either<Failure, Type>> call(Params params);
}

/// When doesn't have params
class NoParams extends Equatable {
  @override
  List<Object> get props => [];
}
