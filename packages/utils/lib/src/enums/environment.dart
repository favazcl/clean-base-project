/// Env
enum Environment {
  /// Develop environment
  dev,

  /// Stage environment
  stg,

  /// Production environment
  prod,
}
