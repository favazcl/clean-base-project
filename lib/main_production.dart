import 'package:clean_base_project/app/app.dart';
import 'package:clean_base_project/bootstrap.dart';
import 'package:clean_base_project/core/injector/injector_container.dart' as di;
import 'package:flutter/material.dart';
import 'package:utils/utils.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.setup();
  AppConfiguration.instance.setEnvironment(Environment.prod);

  await bootstrap(() => const App());
}
