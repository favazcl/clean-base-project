import 'package:domain/domain.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

final GetIt getIt = GetIt.instance;

Future<void> setup() async {
  /// Usecases
  getIt.registerLazySingleton(() => GetDogsUseCase(getIt()));

  /// Repositories
  // ignore: cascade_invocations
  getIt.registerLazySingleton<DogRepository>(
    () => DogRepositoryImpl(dogDataSource: getIt()),
  );

  /// Remote datasources
  // ignore: cascade_invocations
  getIt.registerLazySingleton<DogDataSource>(
    () => DogDataSourceImpl(client: getIt()),
  );

  /// Common
  // ignore: cascade_invocations
  getIt.registerLazySingleton(() => http.Client());
}
