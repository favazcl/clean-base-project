import 'package:bloc/bloc.dart';
import 'package:domain/domain.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:utils/utils.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc({
    required GetDogsUseCase getDogsUseCase,
  })  : _getDogsUseCase = getDogsUseCase,
        super(HomeInitial()) {
    on<DogsRequested>(_onDogsRequested);
  }

  final GetDogsUseCase _getDogsUseCase;

  Future<void> _onDogsRequested(
    DogsRequested event,
    Emitter<HomeState> emit,
  ) async {
    emit(HomeLoading());

    final response = await _getDogsUseCase.call(NoParams());

    response.fold(
      (failure) => emit(HomeError('$failure')),
      (dogs) => emit(HomeLoaded(dogs)),
    );
  }
}
