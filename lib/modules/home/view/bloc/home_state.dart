part of 'home_bloc.dart';

@immutable
abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object?> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HomeLoaded extends HomeState {
  const HomeLoaded(this.dogs);

  final List<DogEntity> dogs;
}

class HomeError extends HomeState {
  const HomeError(this.message);
  
  final String? message;
}
