import 'package:cached_network_image/cached_network_image.dart';
import 'package:clean_base_project/core/injector/injector_container.dart';
import 'package:clean_base_project/modules/home/view/bloc/home_bloc.dart';
import 'package:design_kit/design_kit.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => HomeBloc(
        getDogsUseCase: getIt<GetDogsUseCase>(),
      ),
      child: const HomeView(),
    );
  }
}

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.1),
      body: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          if (state is HomeInitial) {
            return Center(
              child: GenericButton(
                btnColor: Colors.yellowAccent,
                onTap: () {
                  context.read<HomeBloc>().add(const DogsRequested());
                },
                child: const Center(
                  child: Text(
                    'Click para ver perros',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            );
          } else if (state is HomeLoading) {
            return const Center(child:  CircularProgressIndicator());
          } else if (state is HomeError) {
            return const Text('Ha ocurrido un error');
          } else if (state is HomeLoaded) {
            return ListView.builder(
              itemCount: state.dogs.length,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                final dog = state.dogs[index];

                return Card(
                  child: Column(
                    children: [
                      CachedNetworkImage(
                        cacheKey: dog.image.url,
                        memCacheHeight: 800,
                        imageUrl: dog.image.url,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) =>
                                CircularProgressIndicator(
                          value: downloadProgress.progress,
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        dog.name,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        dog.description!.isEmpty
                            ? 'Sin descripción'
                            : dog.description!,
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                );
              },
            );
          }

          return const SizedBox.shrink();
        },
      ),
    );
  }
}
